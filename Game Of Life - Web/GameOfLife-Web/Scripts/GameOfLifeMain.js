﻿var game_width = 0;
var game_height = 0;

var timer;

var worker = new Worker("Scripts/GameOfLifeWorker.js");

var level;

window.onload = function ()
{
    // Höhe und Breite der Fläche (Canvas) in Pixeln ermitteln und
    // in den entsprechenden Variablen speichern
    game_width = document.getElementById("game_canvas").width;
    game_height = document.getElementById("game_canvas").height;

    var feldGrößeX = 50;
    var y = 50;

    level = new Level(feldGrößeX, y);

    worker.postMessage("start:" + feldGrößeX + "," + y);

    drawLevel();

    // Buttons
    document.getElementById('step').onclick = update;

    document.getElementById('start').onclick = start;

    document.getElementById('stop').onclick = stop;

    document.getElementById('game_canvas').onclick = handleUserClick;

    document.getElementById('reset').onclick = reset;

    document.getElementById('save').onclick = save;

    document.getElementById('load').onclick = load;

    document.getElementById('random').onclick = random;

    // Nachrichten aus dem Worker
    // hier hat sich eine Menge geändert.
    console.log("guckuck");
    worker.addEventListener("message", function (event)
    {
        level.allCells = event.data;

        drawLevel();
    });
};

// Konstruktor
function Level(xSize, ySize)
{
    this.xMax = xSize;
    this.yMax = ySize;
    this.size = xSize;
    this.allCells = [];

    // Zellen erzeugen
    for (var i = 0; i < this.xMax; i++)
    {
        this.allCells[i] = [];
        for (var j = 0; j < this.yMax; j++)
        {
            this.allCells[i][j] = false;
        }
    }
}

function start()
{
    // Update alle 250 Millisekunden
    timer = setInterval(update, 250);
}

function stop()
{
    clearInterval(timer);
}

function update()
{
    worker.postMessage("update");

    worker.postMessage("step");
}

function random()
{
    worker.postMessage("random");
    //update();
}

function reset()
{
    for (var i = 0; i < level.xMax; i++)
    {
        for (var j = 0; j < level.yMax; j++)
        {
            level.allCells[i][j] = false;
        }
    }

    worker.postMessage("reset");

    stop();

    drawLevel();
}

function load()
{
    reset();

    for (var i = 0; i < level.xMax; i++)
    {
        for (var j = 0; j < level.yMax; j++)
        {
            // Zustand der Zelle laden
            level.allCells[i][j] = localStorage.getItem("r" + i + "c" + j) === "true";
        }
    }

    // Zustand der Welt an den Worker übertragen
    worker.postMessage("welt:" + level.allCells);

    // Zeichnen
    drawLevel();
}

function save()
{
    for (var i = 0; i < level.xMax; i++)
    {
        for (var j = 0; j < level.yMax; j++)
        {
            // Zustand der Zelle speichern
            localStorage.setItem("r" + i + "c" + j, level.allCells[i][j]);
        }
    }
}

function drawLevel()
{
    // Canvas-Element ermitteln
    var ctx = document.getElementById("game_canvas").getContext("2d");

    // Canvas löschen
    ctx.clearRect(0, 0, game_width, game_height);

    ctx.lineWidth = 2;

    var i;

    // vertikale Trennlinien zeichnen
    for (i = 0; i < level.yMax; i++)
    {
        ctx.beginPath();
        ctx.moveTo(i * (game_width / level.yMax), 0);
        ctx.lineTo(i * (game_width / level.yMax), game_height);
        ctx.stroke();
    }
    // horizontale Trennlinien zeichnen
    for (i = 0; i < level.xMax; i++)
    {
        ctx.beginPath();
        ctx.moveTo(0, i * (game_height / level.xMax));
        ctx.lineTo(game_width, i * (game_height / level.xMax));
        ctx.stroke();
    }

    // Höhe und Breite der Zellen
    var rect_height = (game_height / level.yMax) - 2;
    var rect_width = (game_width / level.xMax) - 2;

    // Zellen zeichnen
    for (i = 0; i < level.xMax; i++)
    {
        for (var j = 0; j < level.yMax; j++)
        {
            if (level.allCells[i][j])
            {
                ctx.fillStyle = "green";
                // Grünes Rechteck für lebende Zelle
                ctx.fillRect(j * (game_width / level.xMax) + 1, i * (game_height / level.yMax) + 1, rect_width, rect_height);
            }
        }
    }
}

function handleUserClick(e)
{
    // Position der Maus relativ zum Canvas ermitteln
    var posX = Math.floor(e.pageX - document.getElementById("game_canvas").getBoundingClientRect().left);
    var posY = Math.floor(e.pageY - document.getElementById("game_canvas").getBoundingClientRect().top);

    // Logische Zeile und Spalte aus der Mausposition ermitteln
    var row = Math.floor(posY / (game_height / level.yMax));
    var col = Math.floor(posX / (game_width / level.xMax));

    // Zustanden austauschen
    level.allCells[row][col] = !level.allCells[row][col];

    // Zustand der Welt an den Worker übertragen
    worker.postMessage("welt:" + level.allCells);

    drawLevel();
}