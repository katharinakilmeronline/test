﻿var level;

self.onmessage = message;

function message(event)
{
    "use strict";
    var nachricht = event.data;
    var kommando = nachricht.split(":")[0];
    var daten = nachricht.split(":")[1];

    //console.log("kommando: " + kommando);
    //console.log("daten: " + daten);

    switch (kommando)
    {
        case "start":
            var x = daten.split(",")[0];
            var y = daten.split(",")[1];
            level = new Level(x, y);
            init();
            break;
        case "update":
            update();
            break;
        case "reset":
            reset();
            break;
        case "step":
            self.postMessage(level.exportArray());
            break;
        case "random":
            level.setRandomCells();
            update();
            break;
        case "welt":
            var boolArray = daten.split(',');
            y = 0;
            for (var i = 0; i < level.xMax; i++)
            {
                for (var j = 0; j < level.yMax; j++)
                {
                    level.allCells[i][j].isAlive = (boolArray[y] === "true");
                    y++;
                }
            }
            break;
        default:
    }
}

function init()
{
    // Startzellen belegen
    level.setRandomCells();
    update();
}

function update()
{
    // Neuen Zustand der Welt bestimmen
    level.cycle();
}

function reset()
{
    for (var i = 0; i < level.xMax; i++)
    {
        for (var j = 0; j < level.yMax; j++)
        {
            level.allCells[i][j].isAlive = false;
        }
    }
}

// Konstruktorfunktion
function Level(xSize, ySize)
{
    this.xMax = xSize;
    this.yMax = ySize;
    this.size = xSize;
    this.allCells = [];
    var i;
    var j;

    // Zellen erzeugen
    for (i = 0; i < this.xMax; i++)
    {
        this.allCells[i] = [];
        for (j = 0; j < this.yMax; j++)
        {
            this.allCells[i][j] = new Cell({ X: i, Y: j });
        }
    }

    // Nachbarn setzen
    for (i = 0; i < this.xMax; i++)
    {
        for (j = 0; j < this.yMax; j++)
        {
            this.allCells[i][j].setNeighbours(this);
        }
    }

    this.setRandomCells = function ()
    {
        var anzahl = this.xMax * this.yMax / Math.floor((Math.random() * 5) + 2);
        for (var i = 0; i < anzahl; i++)
        {
            var x = Math.floor((Math.random() * this.xMax - 1) + 1);
            var y = Math.floor((Math.random() * this.yMax - 1) + 1);
            this.allCells[x][y].isAlive = true;
        }
    };

    this.cycle = function ()
    {
        for (i = 0; i < this.xMax; i++)
        {
            for (j = 0; j < this.yMax; j++)
            {
                if (this.allCells[i][j].isAlive)
                {
                    // Regel 2
                    if (this.allCells[i][j].lifeNeighbourCount() < 2)
                    {
                        this.allCells[i][j].isNextAlive = false;
                    }
                        // Regel 4
                    else if (this.allCells[i][j].lifeNeighbourCount() > 3)
                    {
                        this.allCells[i][j].isNextAlive = false;
                    }
                    else
                    {
                        // Regel 3
                        this.allCells[i][j].isNextAlive = true;
                    }
                }
                else
                {
                    // Regel 1
                    if (this.allCells[i][j].lifeNeighbourCount() === 3)
                    {
                        this.allCells[i][j].isNextAlive = true;
                    }
                    else
                    {
                        this.allCells[i][j].isNextAlive = false;
                    }
                }
            }
        }

        // Nächste Generation
        for (i = 0; i < this.xMax; i++)
        {
            for (j = 0; j < this.yMax; j++)
            {
                this.allCells[i][j].isAlive = this.allCells[i][j].isNextAlive;
            }
        }
    };

    this.exportArray = function ()
    {
        var boolArray = [];
        for (i = 0; i < this.xMax; i++)
        {
            boolArray[i] = [];
            for (j = 0; j < this.yMax; j++)
            {
                boolArray[i][j] = this.allCells[i][j].isAlive;
            }
        }
        //console.log(boolArray);
        return boolArray;
    };

    // DEBUG
    this.print = function ()
    {
        var zeile = "";
        for (i = 0; i < this.xMax; i++)
        {
            for (j = 0; j < this.yMax; j++)
            {
                if (this.allCells[i][j] != null)
                {
                    zeile += this.allCells[i][j];
                }
            }
            console.log(zeile);
            zeile = "";
        }
    };
}

function Cell(position)
{
    this.isAlive;

    this.isNextAlive;

    this.position = position;

    this.neighbours = [];

    this.setNeighbours = function (level)
    {
        if (position.X > 0 && position.Y > 0)
        {
            this.neighbours[0] = level.allCells[position.X - 1][position.Y - 1]; // Top Left
        }

        if (position.Y > 0)
        {
            this.neighbours[1] = level.allCells[position.X][position.Y - 1]; // Top
        }

        if (position.Y > 0 && position.X < level.size - 1)
        {
            this.neighbours[2] = level.allCells[position.X + 1][position.Y - 1]; // Top Right
        }

        if (position.X > 0)
        {
            this.neighbours[3] = level.allCells[position.X - 1][position.Y]; // Left
        }

        if (position.X < level.size - 1)
        {
            this.neighbours[4] = level.allCells[position.X + 1][position.Y]; // Right
        }

        if (position.X > 0 && position.Y < level.size - 1)
        {
            this.neighbours[5] = level.allCells[position.X - 1][position.Y + 1]; // Bottom Left
        }

        if (position.Y < level.size - 1)
        {
            this.neighbours[6] = level.allCells[position.X][position.Y + 1]; // Bottom
        }

        if (position.X < level.size - 1 && position.Y < level.size - 1)
        {
            this.neighbours[7] = level.allCells[position.X + 1][position.Y + 1]; // Bottom Right
        }
    };

    this.lifeNeighbourCount = function ()
    {
        var count = 0;
        for (var i = 0; i < this.neighbours.length; i++)
        {
            if (this.neighbours[i] != null && this.neighbours[i].isAlive)
            {
                count++;
            }
        }
        return count;
    };

    // DEBUG
    this.toString = function ()
    {
        return this.isAlive ? "[#] " : "[ ] ";
    };
}